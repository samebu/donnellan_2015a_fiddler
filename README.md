# Introduction

This repository contains the scripts detailed in the publication:


**Donnellan, S.C., Foster, R., Junge, C., Huveneers, C., Kilian, A. and Bertozzi, T. (2015) Fiddling with the proof: the Magpie Fiddler Ray is a colour pattern variant of the common Southern Fiddler Ray (Rhinobatidae: Trygonorrhina). _Zootaxa_ 3981(3):367-84**


If you use these scripts (or modified versions) in your work, then please cite the publication.


#Contents
**DArT_filter.pl** - PERL script to filter DArT RAD data   
**DArT2minal.pl** - PERL script to encode SNP data based on the number of minor alleles   
**fiddler_transpose.R** - R script to transpose SNP matrix  
**fiddler_analysis.R** - Main analysis R script