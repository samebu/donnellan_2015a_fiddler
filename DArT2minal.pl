#!/usr/bin/perl -w

# dart2minal.pl
# Terry Bertozzi 12.ii.2015
# 
# Convert a transposed DArT SNP matrix (ouput from "fiddler_transpose.R") to a SNP matrix recoded according to the number
# or minor alleles for importing with the R script "fiddler_analysis.R". The script also outputs concatenated SNP strings
# (with iupac codes) for further analysis.
# NOTE: output in same sample input order
#
# usage: dart2minal.pl <input.csv>
#
# This script is referenced in the publication:
#
# Donnellan, S.C., Foster, R., Junge, C., Huveneers, C., Kilian, A. and Bertozzi, T. (2015) 
# Fiddling with the proof: the Magpie Fiddler Ray is a colour pattern variant of the common 
# Southern Fiddler Ray (Rhinobatidae: Trygonorrhina). _Zootaxa_ (submitted)

use strict;
use File::Basename;
use List::Util qw(sum);

my %iupac=('A'=>{'C'=>'M','G'=>'R','T'=>'W'},
           'C'=>{'A'=>'M','G'=>'S','T'=>'Y'},
           'G'=>{'A'=>'R','C'=>'S','T'=>'K'},
           'T'=>{'A'=>'W','C'=>'Y','G'=>'K'}
          );
my %HoA;
my %SNPstring;
my %warnings;
my @loci;
my @alleles;
my @genotype;
my @samples;
my @SNPstring;

unless (open (IN, '<:crlf', $ARGV[0])) { #use PerlIO layer to handle windows files
    die ("Can't open input file $ARGV[0]\n");
}

while (my $line = <IN>) {
    
	# skip the header line
	next if($line=~/^,/);

    # build the locus list
    if ($line=~ /^CloneID/){
        chomp $line;
        my @temp = split(/,/,$line);
        for (my $i=1; $i < @temp; $i +=2){
            push @loci, $temp[$i];
        }
        next;
    }

    # build the allele list
    if ($line=~ /^SNP/){
        my @temp = split(/,/,$line);
        for (my $i=2; $i < @temp; $i +=2){
            my ($pos, $allele)=split(/:/,$temp[$i]);
            $allele=~tr/>/\//;
            push @alleles, $allele;
        }
		next;
    }
    
    # process each sample
    @genotype=();
    @SNPstring=();
    chomp $line;
    my @temp = split(/,/,$line);
    my $id = shift @temp;
    push @samples, $id;
       
    # covert the genotypes to (0, 1, 2, NA) format based on number of minor alleles and
    # build the SNPstring
    for (my $i=0; $i < @temp; $i +=2){
        my $a = $temp[$i];
        my $b = $temp[$i+1];
        
        my ($snp1,$snp2)=split(/\//, $alleles[$i/2]);
      
        if ($a eq 1 && $b eq 0 ) {
            push @genotype, 0;
            push @SNPstring, $snp1;
        }elsif ($a eq 1 && $b eq 1 ) {
            push @genotype, 1;
            my $iupac = get_iupac($snp1,$snp2);
            push @SNPstring, $iupac;
        }elsif ($a eq 0 && $b eq 1 ) {
            push @genotype, 2;
            push @SNPstring, $snp2;
        }elsif ($a eq "-" && $b eq "-") {
            push @genotype, "NA";
            push @SNPstring, "N";
        }else{
            print "Possible error in conversion for $id ($a : $b)"
        }
    }
   
    $HoA{$id} =[@genotype];
    $SNPstring{$id}=join("",@SNPstring);
}

close IN;

my ($fname, $dir, $ext) = fileparse($ARGV[0],'_torecode\..*');
my $outfile=$fname."_recoded.csv";
my $outfile1=$fname."_alleles.csv";
my $outfile2=$fname."_SNPstrings.fasta";

unless(open(OUT,">",$outfile)){
        die ("Unable to open $outfile\n")
}

unless(open(OUT1,">",$outfile1)){
        die ("Unable to open $outfile1\n")
}

unless(open(OUT2,">",$outfile2)){
        die ("Unable to open $outfile2\n")
}
 
# write out the sorted recoded file
unshift @loci,"sample"; 
print OUT join(',', @loci);
print OUT "\n";
foreach my $key (sort keys %HoA){
    print OUT "$key,".join(",",@{$HoA{$key}});
    print OUT "\n";
}

#write out the SNPstring fasta file
foreach my $key(sort keys %SNPstring){
    print OUT2 ">$key\n$SNPstring{$key}\n";
}

#write out the alleles
print OUT1 join(",",@alleles);

close OUT;
close OUT1;
close OUT2;

# print out the multilevel hash so that the same warning types are together
if (%warnings){
    unless (open(WARN,">","$outfile.warnings")){
        die ("Can't open outfile $outfile.warnings\n");
    }
    print WARN "Please check the following potential problems:\n";
    foreach my $warning (sort keys %warnings){
        print WARN "$warning:\n";
        foreach my $warn (sort keys %{$warnings{$warning}}){
            print WARN "\t$warn ".$warnings{$warning}{$warn};
            print WARN "\n";
        }
    }
    close WARN;
}


# this subroutine takes two heterozygous strings and returns
# the string as IUPAC ambiguity codes
sub get_iupac { 
    my ($str1, $str2)= @_;
    my @iupac;
    my @str1=split(//,$str1);
    my @str2=split(//,$str2);
    
    while (@str1) {
        my $base1=shift @str1;
        my $base2=shift @str2;
        if (exists($iupac{$base1}) && exists($iupac{$base1}{$base2})){ # check for spurious bases
            push @iupac, $iupac{$base1}{$base2};
        }else{
            $warnings{'bad_base'}="$str1,$str2";
            push @iupac, $base1; #leave the ref allele unchanged
        }
    }
    return join("",@iupac);
}
